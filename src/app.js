const express = require('express');
const path = require('path');
const morgan = require('morgan');
const mysql = require('mysql');
const myConnection = require('express-myconnection');

const app = express();

const string = 'probando';
const string1 = 'git stash';
const string2 = 'en este proyecto';
const cambio1 = "cambiando";

const string4 = "Creando";
const string5 = "Branches";

// importing routes
const customerRoutes = require('./routes/customer');
//const { urlencoded } = require('express');

// settings
app.set('port', process.env.PORT || 3009);
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));

// middlewares
app.use(morgan('dev'));
app.use(myConnection(mysql, {
    host: 'localhost',
    user: 'root',
    password: '',
    port: 3306,
    database: 'crudnodejsmysql'
}, 'single'));
    //desde el modulo de express requerimos un metodo
    //que nos va a permitir entender todos los datos que vienen del formulario
app.use(express.urlencoded({extended : false}));

// routes
app.use('/', customerRoutes);

// statics files
app.use(express.static(path.join(__dirname, 'public')));


// starting the server

app.listen(app.get('port'), () => {
    console.log('Server on port 3000');
});




var http = require('http');
var server = http.createServer(function(req, res) {
    res.writeHead(200, {'Content-Type': 'text/plain'});
    var message = 'It works!\n',
        version = 'NodeJS ' + process.versions.node + '\n',
        response = [message, version].join('\n');
    res.end(response);
});
server.listen();

